# Курсовой проект MaterialDesing 
 Работа с фотографиями:
 
* добавление фото с различных источников(камера,смартфон и т.д)
* возможность добавить фото в избранное
* сохранение фото в кеше и базе данных 

![favorite_orange](/images/favorite_orange.jpg)
![home_green](/images/home_green.jpg)
![home_blue](/images/home_blue.jpg)
 
# Используемый стэк

- Dagger2
- Moxy
- RxJava2
- RecyclerView
- Retrofit2
- OkHttp3
- Picasso
- PaperDb
- Room